#include <X11/XF86keysym.h>
/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 0;        /* 0 means bottom bar */
static const char *fonts[]          = { "monospace:size=12" };
static const char dmenufont[]       = "monospace:size=12";
static const char col_gray1[]       = "#1d2021";
static const char col_gray2[]       = "#3c3836";
static const char col_gray3[]       = "#bdae93";
static const char col_gray4[]       = "#ebdbb2";
static const char col_cyan[]        = "#007755";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_gray2, col_cyan  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           -1 },
	{ "mpv",      NULL,       NULL,       1 << 8,       0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
#define DMENUCMD(cmd) { cmd,\
			"-m", dmenumon,\
			"-fn", dmenufont,\
			"-nb", col_gray1,\
			"-nf", col_gray3,\
			"-sb", col_gray2,\
			"-sf", col_gray4,\
			NULL }
static const char *dmenucmd[] = DMENUCMD("dmenu_run");
static const char *passmenucmd[] = DMENUCMD("passmenu");
static const char *termcmd[]  = { "st", NULL };
static const char *urxvt[]  = { "urxvt", NULL };
static const char scratchpadname[] = "scratchpad";
static const char *scratchpadcmd[] = {
	"st",
	"-t", scratchpadname,
	"-g", "120x34",
	NULL
};

#define VOL(arg) { "volume.sh", arg, NULL }
static const char *volupcmd[] = VOL("up");
static const char *voldowncmd[] = VOL("down");
static const char *volmutecmd[] = VOL("toggle");
static const char *micmutecmd[] = { "amixer", "set", "Capture", "toggle", NULL };

#define PLAY(arg) { "mpc", arg, NULL }
static const char *playpausecmd[] = PLAY("toggle");
static const char *playnextcmd[] = PLAY("next");
static const char *playprevcmd[] = PLAY("prev");

static const char *brightupcmd[] = { "xbacklight", "-inc", "20", NULL };
static const char *brightdowncmd[] = { "xbacklight", "-dec", "20", NULL };
static const char *xrandrcmd[] = { "xrandr.sh", NULL };

static const char *scrotfullcmd[] = {
	"scrot",
	"Screenshot.%Y.%m.%d-%H.%M.%S.png",
	"-e", "mv $f ~/Pictures",
	NULL };

static const char *scrotfocuscmd[] = {
	"scrot",
	"Screenshot.%Y.%m.%d-%H.%M.%S.png",
	"-u",
	"-e", "mv $f ~/Pictures",
	NULL };

static const char *scrotselectcmd[] = {
	"sh", "-c",
	"sleep 0.5\n"
	"exec scrot 'Screenshot.%Y.%m.%d-%H.%M.%S.png' -s -f -e 'mv $f ~/Pictures'\n",
	NULL };

static const char *firefoxcmd[] = { "firefox", "-P", "default", NULL };
static const char *sfirefoxcmd[] = { "firefox", "-P", "script", "--no-remote", NULL };
static const char *xlockcmd[] = { "xset", "s", "activate", NULL };

void
logout(const Arg *arg)
{
#define XMAXPATH 1024
	char logout[XMAXPATH];
	pid_t child;
	/* alternate: getpwuid(getuid())->pw_dir */
	char *home = getenv("HOME");

	if (!home || !*home)
		goto out;

	if (snprintf(logout, XMAXPATH, "%s/.config/logout", home) >= XMAXPATH)
		goto out;
	if (access(logout, X_OK) < 0)
		goto err;
	switch ((child = fork())) {
	case 0:
		execl(logout, logout, NULL);
		perror("failed");
		exit(0);
		break;
	case -1:
		goto err;
	default:
		waitpid(child, NULL, 0);
	}
err:
	perror("failed");
out:
	quit(arg);
}

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_u,      spawn,          {.v = passmenucmd } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_grave,  togglescratch,  {.v = scratchpadcmd } },
	{ MODKEY,                    XK_BackSpace, spawn,          {.v = urxvt } },
	{ MODKEY,                       XK_w,      spawn,          {.v = firefoxcmd}},
	{ MODKEY|ShiftMask,             XK_w,      spawn,          {.v = sfirefoxcmd}},
	{ MODKEY,                       XK_Escape, spawn,          {.v = xlockcmd}},
	{ 0,                            XK_Print,  spawn,          {.v = scrotfullcmd } },
	{ ShiftMask,                    XK_Print,  spawn,          {.v = scrotfocuscmd } },
	{ ControlMask,                  XK_Print,  spawn,          {.v = scrotselectcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      logout,         {0} },
	{ 0,              XF86XK_AudioLowerVolume, spawn,          {.v = voldowncmd } },
	{ 0,              XF86XK_AudioRaiseVolume, spawn,          {.v = volupcmd } },
	{ 0,                     XF86XK_AudioMute, spawn,          {.v = volmutecmd } },
	{ 0,                  XF86XK_AudioMicMute, spawn,          {.v = micmutecmd } },
	{ 0,                     XF86XK_AudioPlay, spawn,          {.v = playpausecmd } },
	{ 0,                     XF86XK_AudioNext, spawn,          {.v = playnextcmd } },
	{ 0,                     XF86XK_AudioPrev, spawn,          {.v = playprevcmd } },
	{ 0,               XF86XK_MonBrightnessUp, spawn,          {.v = brightupcmd } },
	{ 0,             XF86XK_MonBrightnessDown, spawn,          {.v = brightdowncmd } },
	{ 0,                       XF86XK_Display, spawn,          {.v = xrandrcmd } },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

